package com.stoufam.hp.antriin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.stoufam.hp.antriin.Config.ROOT_URL;

public class activity_detail_bank extends AppCompatActivity {

    class_custompager viewPager;
    private Toolbar toolbar;
    TabLayout tabLayout;
    private TextView[] textViews = new TextView[3];

    private ViewPagerAdapter adapter;

    String nim = "";
    String id_bank = "";
    public static final String terima = "data";

    private TextView namaBank, jalanBank;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_bank_2);

        viewPager = (class_custompager) findViewById(R.id.view_pager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);

        namaBank = (TextView) findViewById(R.id.nama_bank);
        jalanBank = (TextView) findViewById(R.id.jalan_bank);

        setup_tab();
        Intent intent = getIntent();
        id_bank = intent.getStringExtra(Config.EMP_ID);
        ShowData();
    }

    private void ShowData() {
        //Ketika Aplikasi mengambil data kita akan melihat progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Fetching Data", "Please Wait..", false, false);
        loading.setCancelable(true);
        //Logging Interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set Level Log
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())//GsonConverter untuk parsing json
                .client(httpClient.build())
                .build();

        HashMap<String, String> params = new HashMap<>();
        params.put("ID_BANK", id_bank);

        RestAPI service = retrofit.create(RestAPI.class);
        Call<List<model_detail_bank>> call = service.loadDetailBank(params);
        call.enqueue(new Callback<List<model_detail_bank>>() {  //Asyncronous Request
            @Override
            public void onResponse(Call<List<model_detail_bank>> call, Response<List<model_detail_bank>> response) {
                loading.dismiss();

                namaBank.setText(response.body().get(0).getNamaBank());
                jalanBank.setText(response.body().get(0).getLatitude());
//                kabupaten.setText(buku.get(0).getKabupaten());
//                kecamatan.setText(buku.get(0).getKecamatan());
//                kamar_kosong.setText(buku.get(0).getKamarkosong());
//                jenis_penghuni.setText(buku.get(0).getJenispenghuni());
//                alamat.setText(buku.get(0).getAlamat());
//                detai_kost.setText(buku.get(0).getDeskripsi());
//                harga.setText(buku.get(0).getHarga());
//                lat =Double.parseDouble(buku.get(0).getLatitude());
//                lang = Double.parseDouble(buku.get(0).getLongitude());
//                //memanggil method untuk menampilkan list
//                // LatLng latLng = null;
//                googleMap=((MapFragment)getFragmentManager().findFragmentById(R.id.mapp)).getMap();
//                LatLng latLng = new LatLng(lat,lang);
//                Marker marker = googleMap.addMarker(new MarkerOptions()
//                        .position(latLng)
//                        .title(buku.get(0).getNamakost())
//                        .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("marker",100,100)))
//
//                );
//                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
//                CameraPosition cameraPosition = new CameraPosition.Builder()
//                        .target(latLng)             // Sets the center of the map to location user
//                        .zoom(18)                   // Sets the zoom
//                        .bearing(90)                // Sets the orientation of the camera to east
//                        .tilt(40)                   // Sets the tilt of the camera to 30 degrees
//                        .build();                   // Creates a CameraPosition from the builder
//                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                googleMap.getUiSettings().setZoomControlsEnabled(true);
            }

            @Override
            public void onFailure(Call<List<model_detail_bank>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Gagal mengambil data", Toast.LENGTH_LONG).show();
            }
        });
    }

    void setup_tab() {
        TextView tab1 = (TextView) LayoutInflater.from(this).inflate(R.layout.home_tab, null);
        tab1.setText("TELLER");
        tabLayout.getTabAt(0).setCustomView(tab1);

        TextView tab2 = (TextView) LayoutInflater.from(this).inflate(R.layout.home_tab, null);
        tab2.setText("CUSTOMER SERVICE");
        tabLayout.getTabAt(1).setCustomView(tab2);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFrag(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    private void setupViewPager(class_custompager viewPager) {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new fragment_teller(), "ONE");
        adapter.addFrag(new fragment_cs(), "TWO");
        viewPager.setAdapter(adapter);

    }
}
