package com.stoufam.hp.antriin;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;

public class class_prosessql {
    Context ctx1;
    String[] text_menu;
    String[] url_menu;

    public class_prosessql(Context ctx){
        this.ctx1=ctx;
    }
    public void insertdatamember(String username,
                                 String first_name,
                                 String last_name,
                                 String email){
        class_helpersqlite db_helper = new class_helpersqlite(ctx1);
        SQLiteDatabase db = db_helper.getWritableDatabase();
        SQLiteStatement stmt = db.compileStatement("insert into tbl_member (" +
                "username," +
                "first_name," +
                "last_name," +
                "email)values(?,?,?,?)");
        stmt.bindString(1, username);
        stmt.bindString(2, first_name);
        stmt.bindString(3, last_name);
        stmt.bindString(4, email);
//        stmt.bindString(5, id_akses);
//        stmt.bindString(6, id_tipemember);
//        stmt.bindString(7, status_verified);
        stmt.execute();
        stmt.close();
        db.close();
    }

    public String cek_session(){
        String ada="";
        class_helpersqlite db_helper=new class_helpersqlite(ctx1);
        SQLiteDatabase db=db_helper.getWritableDatabase();
        Cursor c=db.rawQuery("select * from tbl_member",null);
        if(c.moveToFirst())
        {
            ada="ada";
        }
        else
        {
            ada="kosong";
        }
        c.close();
        db.close();
        return ada;
    }

    public void hapusdatamember() {
        class_helpersqlite db_helper=new class_helpersqlite(ctx1);
        SQLiteDatabase db=db_helper.getWritableDatabase();
        SQLiteStatement stmt=db.compileStatement("delete from tbl_member");
        stmt.execute();
        stmt.close();
        db.close();
    }

    public String[] bacadatamember(){
        String[]output=new String[7];
        class_helpersqlite db_helper=new class_helpersqlite(ctx1);
        SQLiteDatabase db=db_helper.getWritableDatabase();
        Cursor c = db.rawQuery("select * from tbl_member", null);

        if (c.moveToFirst()) {
            int i=0;
            do {
                output[0]=c.getString(0);
                output[1]=c.getString(1);
                output[2]=c.getString(2);
                output[3]=c.getString(3);
//                output[4]=c.getString(4);
//                output[5]=c.getString(5);
//                output[6]=c.getString(6);
                i++;
            } while (c.moveToNext());
        }
        c.close();
        return output;
    }
}
