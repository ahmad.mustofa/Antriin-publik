package com.stoufam.hp.antriin;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.stoufam.hp.antriin.Config.ROOT_URL;

public class activity_my_profil extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private Menu menu;
    private Toolbar toolbar;
    private EditText[] textViews = new EditText[5];
    private CircleImageView circleImageView;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private FloatingActionButton floatingActionButton;
    private TextView tvEdit, batal, simpan;
    String nama = "";
    String mobilePhone = "";
    String email = "";
    String nim = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profil);

        inisialisasi();
        setup_actionbar();

        nim = new class_prosessql(this).bacadatamember()[0].toString();

        ambilData();

    }

    void inisialisasi() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (new class_prosessql(this).cek_session().equals("ada")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_drawer_logged);
//            if (new class_prosessql(this).bacadatamember()[4].equals("1555003")) {
//                navigationView.getMenu().getItem(1).setChecked(true);
//            }
        }
        navigationView.setNavigationItemSelectedListener(this);

        tvEdit = (TextView) findViewById(R.id.tv_edit);

        batal = (TextView) findViewById(R.id.batal);
        simpan = (TextView) findViewById(R.id.simpan);

        textViews[0] = (EditText) findViewById(R.id.tv_isi_nama_hal_18);
        textViews[1] = (EditText) findViewById(R.id.tv_phone_hal_18);
        textViews[2] = (EditText) findViewById(R.id.tv_email_hal_18);
//        circleImageView = (CircleImageView) findViewById(R.id.image_owner_halaman_14);
//        new class_http().set_profil_new(this, textViews, circleImageView, new class_prosessql(this).bacadatamember()[0]);

//        floatingActionButton = (FloatingActionButton) findViewById(R.id.fb_edit);
    }

    void setup_actionbar() {
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        TextView tv = new TextView(getApplicationContext());

        getSupportActionBar().setTitle("My Profil");
        // Create a LayoutParams for TextView
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView
        // Apply the layout parameters to TextView widget
        tv.setLayoutParams(lp);

        // Set text to display in TextView
        tv.setText(ab.getTitle()); // ActionBar title text

        // Set the text color of TextView to black
        // This line change the ActionBar title text color
        tv.setTextColor(Color.WHITE);

        // Set the TextView text size in dp
        // This will change the ActionBar title text size
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);

        // Set the ActionBar display option
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        // Finally, set the newly created TextView as ActionBar custom view
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    void ambilData() {

        //Ketika Aplikasi mengambil data kita akan melihat progress dialog
        final ProgressDialog loading = ProgressDialog.show(activity_my_profil.this, "Fetching Data", "Please Wait..", false, false);
        loading.setCancelable(true);
        //Logging Interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set Level Log
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())//GsonConverter untuk parsing json
                .client(httpClient.build())
                .build();

        HashMap<String, String> params = new HashMap<>();
        params.put("ID_PELANGGAN", nim);

        RestAPI service = retrofit.create(RestAPI.class);
        Call<List<model_pelanggan>> call = service.loadPelanggan(params);
        call.enqueue(new Callback<List<model_pelanggan>>() {  //Asyncronous Request
            @Override
            public void onResponse(Call<List<model_pelanggan>> call, Response<List<model_pelanggan>> response) {
                loading.dismiss();

                textViews[0].setText(response.body().get(0).getNAMA());
                textViews[1].setText(response.body().get(0).getNOMORHP());
                textViews[2].setText(response.body().get(0).getEMAIL());

            }

            @Override
            public void onFailure(Call<List<model_pelanggan>> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "Gagal mengambil data", Toast.LENGTH_LONG).show();
            }
        });
    }

    void editData() {
        nama = textViews[0].getText().toString();
        mobilePhone = textViews[1].getText().toString();
        email = textViews[2].getText().toString();

        HashMap<String, String> params = new HashMap<>();
        params.put("ID_PELANGGAN", nim);
        params.put("NAMA_PELANGGAN", nama);
        params.put("NOMOR_HP", mobilePhone);
        params.put("EMAIL", email);

        //Ketika Aplikasi mengambil data kita akan melihat progress dialog
        //Logging Interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set Level Log
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())//GsonConverter untuk parsing json
                .client(httpClient.build())
                .build();

        RestAPI api = retrofit.create(RestAPI.class);
        Call<ResponseBody> result = api.updateProfil(params);
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                try {
                    if (response.body() != null)
                        Toast.makeText(activity_my_profil.this, "Profil telah diupdate", Toast.LENGTH_LONG).show();
                        ok();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Toast.makeText(activity_my_profil.this, "Profil gagal diupdate", Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
    }

    void edit(){
        textViews[0].setEnabled(true);
        textViews[1].setEnabled(true);
        textViews[2].setEnabled(true);
        batal.setVisibility(View.VISIBLE);
        simpan.setVisibility(View.VISIBLE);
        tvEdit.setVisibility(View.GONE);
    }

    void ok(){
        textViews[0].setEnabled(false);
        textViews[1].setEnabled(false);
        textViews[2].setEnabled(false);
        batal.setVisibility(View.GONE);
        simpan.setVisibility(View.GONE);
        tvEdit.setVisibility(View.VISIBLE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        class_menu.optiondipilih(menu, id, activity_my_profil.this);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, activity_main.class);
        startActivity(intent);
        finish();
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        class_menu.menudipilih(id, activity_my_profil.this);
        return true;
    }

    public void pindah(View view) {
//        Intent intent = new Intent(activity_my_profil.this, activity_edit_profil.class);
//        startActivity(intent);
        edit();
    }

    public void batal(View view) {
        ok();
    }

    public void simpan(View view) {
        editData();
    }

}
