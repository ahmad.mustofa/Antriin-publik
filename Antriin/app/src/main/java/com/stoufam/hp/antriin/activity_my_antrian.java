package com.stoufam.hp.antriin;

import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class activity_my_antrian extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private Menu menu;
    private Toolbar toolbar;
    private TextView[] textViews = new TextView[5];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_antrian);

        inisialisasi();
        setup_actionbar();
    }

    void inisialisasi() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (new class_prosessql(this).cek_session().equals("ada")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_drawer_logged);
//            if (new class_prosessql(this).bacadatamember()[4].equals("1555003")) {
//                navigationView.getMenu().getItem(1).setChecked(true);
//            }
        }
        navigationView.setNavigationItemSelectedListener(this);

        textViews[0] = (TextView) findViewById(R.id.nama_bank);
//        textViews[1] = (TextView) findViewById(R.id.tv_jumlah_like_halaman_14);
//        textViews[2] = (TextView) findViewById(R.id.tv_jumlah_poin_halaman_14);
//        circleImageView = (CircleImageView) findViewById(R.id.image_owner_halaman_14);
//        new class_http().set_profil_new(this, textViews, circleImageView, new class_prosessql(this).bacadatamember()[0]);
    }

    void setup_actionbar() {
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        TextView tv = new TextView(getApplicationContext());

        getSupportActionBar().setTitle("My Antrian");
        // Create a LayoutParams for TextView
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView
        // Apply the layout parameters to TextView widget
        tv.setLayoutParams(lp);

        // Set text to display in TextView
        tv.setText(ab.getTitle()); // ActionBar title text

        // Set the text color of TextView to black
        // This line change the ActionBar title text color
        tv.setTextColor(Color.WHITE);

        // Set the TextView text size in dp
        // This will change the ActionBar title text size
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);

        // Set the ActionBar display option
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        // Finally, set the newly created TextView as ActionBar custom view
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        class_menu.optiondipilih(menu, id, activity_my_antrian.this);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, activity_main.class);
        startActivity(intent);
        finish();
    }


    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        class_menu.menudipilih(id, activity_my_antrian.this);
        return true;
    }
}
