package com.stoufam.hp.antriin;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.stoufam.hp.antriin.Config.ROOT_URL;

public class activity_main extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    public static final String nim = "nim";
    private boolean doubleBackToExitPressedOnce = false;
    private Toolbar tbar;
    private Menu menu;
    private boolean status_cari = false;

    //    private MaterialSearchView searchView;
    private NavigationView navigationView;

    private GoogleMap googleMap;
    static final LatLng DILO_MALANG = new LatLng(-7.979930, 112.629883);

    //add marker
//    BaseAPIMarker BaseAPIMarkers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        login = (Button) findViewById(R.id.login_button);
//        daftar = (Button) findViewById(R.id.daftar_button);
//        detail = (Button) findViewById(R.id.detail_button);
//        detail2 = (Button) findViewById(R.id.detail_antri);
        tbar = (Toolbar) findViewById(R.id.toolbar);

        inisialisasi();
        set_title();
        marker();
        beranda();
    }

    public void beranda(){
//        took();
        googleMap=((MapFragment)getFragmentManager().findFragmentById(R.id.mapp)).getMap();

//        Marker marker=googleMap.addMarker(new MarkerOptions().position(DILO_MALANG)
//                .icon(BitmapDescriptorFactory.fromResource(R.drawable.bird_icon))
//                .title("UIN MALANG").snippet("TEKNIK INFORMATIKA UIN MALANG"));
        Marker marker = googleMap.addMarker(new MarkerOptions()
                .position(DILO_MALANG)
                .title("UIN MALANG").snippet("TEKNIK INFORMATIKA UIN MALANG"));

        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(DILO_MALANG, 10));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(DILO_MALANG)             // Sets the center of the map to location user
                .zoom(14)                   // Sets the zoom
                .bearing(0)                // Sets the orientation of the camera to east
                .tilt(0)                   // Sets the tilt of the camera to 30 degrees
                .build();                   // Creates a CameraPosition from the builder
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        googleMap.getUiSettings().setZoomControlsEnabled(true);
    }

    public void marker() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set Level Log
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())//GsonConverter untuk parsing json
                .client(httpClient.build())
                .build();

        RestAPI service = retrofit.create(RestAPI.class);
        Call<List<model_bank>> maplist = service.tes();
        maplist.enqueue(new Callback<List<model_bank>>() {
            @Override
            public void onResponse(Call<List<model_bank>> call, final Response<List<model_bank>> response) {
                // String a = String.valueOf(response.body().get(0).getResultLocation().get(1));
                List<model_bank> buku = response.body();
                //String[] items = new String[buku.size()];
                //tvSearch = (EditText) findViewById(R.id.tvSearch);
                LatLng latLng = null;

                googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.mapp)).getMap();
                for (int i = 0; i < buku.size(); i++) {
                    Double lat = Double.parseDouble(buku.get(i).getLatitude());
                    Double longg = Double.parseDouble(buku.get(i).getLongitude());


                    latLng = new LatLng(lat, longg);
                    Marker marker = googleMap.addMarker(new MarkerOptions()
                            .position(latLng)
                            .title(buku.get(i).getNamaBank()).snippet(buku.get(i).getIdBank())
                            .icon(BitmapDescriptorFactory.fromBitmap(resizeMapIcons("marker", 100, 100)))

                    );

                    //googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(UIN_MALANG, 10));

                }
//                googleMap.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
//                CameraPosition cameraPosition = new CameraPosition.Builder()
//                        .target(latLng)             // Sets the center of the map to location user
//                        .zoom(15)                   // Sets the zoom
//                        .bearing(0)                // Sets the orientation of the camera to east
//                        .tilt(40)                   // Sets the tilt of the camera to 30 degrees
//                        .build();                   // Creates a CameraPosition from the builder
//                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                googleMap.getUiSettings().setZoomControlsEnabled(true);

                googleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                    @Override
                    public void onInfoWindowClick(Marker marker) {
                        Intent intent1 = new Intent(activity_main.this, activity_detail_bank.class);
                        String title = marker.getSnippet();
                        intent1.putExtra(Config.EMP_ID, title);
                        intent1.putExtra(activity_detail_bank.terima, title);
//                        Toast.makeText(getApplicationContext(), title, Toast.LENGTH_SHORT).show();
                        startActivity(intent1);
                    }
                });

            }

            @Override
            public void onFailure(Call<List<model_bank>> call, Throwable t) {

            }

        });
    }

    void inisialisasi() {
        class_helpersqlite db_helper = new class_helpersqlite(this);
        SQLiteDatabase db = db_helper.getWritableDatabase();
        db_helper.create_member(db);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (new class_prosessql(this).cek_session().equals("ada")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_drawer_logged);
        } else {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_drawer);
        }

        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setNavigationItemSelectedListener(this);

        tbar = (Toolbar) findViewById(R.id.toolbar);
    }

    void set_title() {
        setSupportActionBar(tbar);
        ActionBar ab = getSupportActionBar();

        getSupportActionBar().setTitle("ANTRIIN");

        TextView tv = new TextView(getApplicationContext());

        // Create a LayoutParams for TextView
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, // Width of TextView
                RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView

        // Apply the layout parameters to TextView widget
        tv.setLayoutParams(lp);

        // Set text to display in TextView
        tv.setText(ab.getTitle()); // ActionBar title text

        // Set the text color of TextView to black
        // This line change the ActionBar title text color
        tv.setTextColor(Color.WHITE);

        // Set the TextView text size in dp
        // This will change the ActionBar title text size
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);

        // Set the ActionBar display option
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        // Finally, set the newly created TextView as ActionBar custom view
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        class_menu.menudipilih(id, activity_main.this);
        return true;
    }

    @Override
    public void onBackPressed(){
//        if (searchView.isSearchOpen()) {
//            searchView.closeSearch();
//        } else {

            Toast.makeText(this, "Press again for close", Toast.LENGTH_SHORT).show();
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
//        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        class_menu.optiondipilih(menu, id, activity_main.this);
        return super.onOptionsItemSelected(item);
    }

    private Bitmap resizeMapIcons(String marker, int i, int i1) {
        Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), getResources().getIdentifier(marker, "drawable", getPackageName()));
        Bitmap re = Bitmap.createScaledBitmap(imageBitmap, i, i1, false);
        return re;
    }

}
