package com.stoufam.hp.antriin;

/**
 * Created by hp on 12/17/2016.
 */

public class Config {

    //Alamat URL tempat kita meletakkan script PHP di PC Server
    // Link untuk INSERT Data
//    public static final String ROOT_URL = "http://antriin.000webhostapp.com/api/";
    public static final String ROOT_URL = "http://192.168.100.7/antriin/api/";


    // Field yang digunakan untuk dikirimkan ke Database, sesuaikan saja dengan Field di Tabel Mahasiswa
    public static final String KEY_EMP_NIM = "nim";
    public static final String KEY_EMP_NAMA = "nama";
    public static final String KEY_EMP_JURUSAN = "jurusan";
    public static final String KEY_EMP_FAKULTAS = "fakultas";
    public static final String KEY_EMP_HP = "hp";
    public static final String KEY_EMP_PASS = "pass";

    // Tags Format JSON
    public static final String TAG_JSON_ARRAY = "result";
    public static final String TAG_NPM = "nim";
    public static final String TAG_NAME = "nama";
    public static final String TAG_JURUSAN = "jurusan";

    //employee id to pass with intent
    // Mahasiswa ID
    public static final String EMP_ID = "emp_id";


    //untuk login

    //url login
    public static final String URL_LOGIN = ROOT_URL + "sepedaku/new_login.php";
    // Variabel untuk definisikan Username dan password methode POST sesuai dengan yang di : new_login.php
    public static final String KEY_LOGIN = "nim";
    public static final String KEY_PASSWORD = "pass";

    // Jika respon server adalah sukses login
    public static final String LOGIN_SUCCESS = "success";

    //Kunci untuk Sharedpreferences
    public static final String SHARED_PREF_NAME = "myloginapp";

    //Ini digunakan untuk store username jika User telah Login
    public static final String EMAIL_SHARED_PREF = "idLogin";

    // Ini digunakan untuk store sharedpreference untuk cek user melakukan login atau tidak
    public static final String LOGGEDIN_SHARED_PREF = "loggedin";
}
