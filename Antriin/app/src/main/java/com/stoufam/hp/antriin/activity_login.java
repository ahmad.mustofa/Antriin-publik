package com.stoufam.hp.antriin;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class activity_login extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    private NavigationView navigationView;
    private Menu menu;
    private Toolbar tbar;
    private Button login_button;
    private TextView register;
    private EditText username, password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.getMenu().getItem(1).setChecked(true);
        navigationView.setNavigationItemSelectedListener(this);

        tbar = (Toolbar) findViewById(R.id.toolbar);
        username = (EditText) findViewById(R.id.username);
        password = (EditText) findViewById(R.id.password);
        login_button = (Button) findViewById(R.id.login_button);
        register = (TextView) findViewById(R.id.textViewDaftar);
        register.setOnClickListener(this);
        login_button.setOnClickListener(this);

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        class_menu.menudipilih(id, activity_login.this);
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == register.getId()) {
            Intent intent = new Intent(this, activity_daftar.class);
            startActivity(intent);
            finish();
        } else if (view.getId() == login_button.getId()) {
            if (username.getText().toString().replace(" ", "").equals("")) {
                Toast.makeText(activity_login.this, "Masukkan username anda", Toast.LENGTH_SHORT).show();
                username.requestFocus();
            } else if (password.getText().toString().replace(" ", "").equals("")) {
                Toast.makeText(activity_login.this, "Masukkan password anda", Toast.LENGTH_SHORT).show();
                password.requestFocus();
            } else {
                new class_http().proses_login(activity_login.this, username.getText().toString().replace(" ", ""), password.getText().toString().replace(" ", ""));
            }
        }
    }
}
