package com.stoufam.hp.antriin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class activity_login_2 extends AppCompatActivity {

    //Definisi views
    private EditText mUsername;
    private EditText mPassword;
    private Button buttonMasuk2;
    private TextView tvDaftar;

    //Tipe Variabel boolean untuk cek User Udah Login atau Tidak
    //Default set dengan False
    private boolean loggedIn = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_2);

        mUsername = (EditText) findViewById(R.id.username);
        mPassword = (EditText) findViewById(R.id.password);

        buttonMasuk2 = (Button) findViewById(R.id.login_button);

        tvDaftar = (TextView) findViewById(R.id.textViewDaftar);

        tvDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), activity_daftar.class));
            }
        });

        //Tambahan Click Listener
        buttonMasuk2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        loggedIn = sharedPreferences.getBoolean(Config.LOGGEDIN_SHARED_PREF, false);

        if (loggedIn) {
            // Class Yang akan muncul jika Login Sukses
            Intent intent;
            intent = new Intent(activity_login_2.this, activity_main.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            intent.putExtra(Config.EMP_ID, Config.KEY_LOGIN);
//            intent.putExtra(activity_main.nim, Config.KEY_LOGIN);
            startActivity(intent);
        }
    }

    private void login() {
        // Ubah ketipe String
        final String IdLogin = mUsername.getText().toString().trim();
        final String PassLogin = mPassword.getText().toString().trim();
//        Buatkan Request Dalam bentuk String
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Config.URL_LOGIN,
                new Response.Listener<String>() {

                    @Override
                    public void onResponse(String response) {
                        //Jika Respon server sukses
                        if (response.equalsIgnoreCase(Config.LOGIN_SUCCESS)) {
                            //Buatkan sebuah shared preference
                            SharedPreferences sharedPreferences = activity_login_2.this.getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);

                            //Buatkan Sebuah variabel Editor Untuk penyimpanan Nilai shared preferences
                            SharedPreferences.Editor editor = sharedPreferences.edit();

                            //Tambahkan Nilai ke Editor
                            editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, true);
                            editor.putString(Config.EMAIL_SHARED_PREF, IdLogin);
                            editor.putString(Config.KEY_LOGIN, IdLogin);

                            //Simpan Nilai ke Variabel editor
                            editor.commit();

                            class_helpersqlite db_helper = new class_helpersqlite(getApplicationContext());
                            SQLiteDatabase db = db_helper.getWritableDatabase();
                            db_helper.delete_member(db);
//                            db.execSQL("insert into member(emailmember, passmember) values('" +
//                                    String.valueOf(emailmemberlogin.getText()) + "','" +
//                                    String.valueOf(passwordmemberlogin.getText()) + "')");

                            //Starting Class yang dipanggil
//                            Intent intent = new Intent(activity_login_2.this, activity_main.class);
//                            String Id = String.valueOf(mUsername.getText());
//                            intent.putExtra(Config.EMP_ID, Id);
//                            intent.putExtra(activity_main.nim, Id);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                            startActivity(intent);
                        } else {
                            //Jika Respon Dari Server tidak Sukses
                            //Tampilkan Pesan Errorrr dengan Toast,,
                            Toast.makeText(activity_login_2.this, "Salah Id dan Password", Toast.LENGTH_LONG).show();
                        }
                    }

                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //Tambahkan apa yang terjadi setelah Pesan Error muncul, alternatif
                        Toast.makeText(getApplicationContext(), "Tidak Bisa Terhubung dengan server", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                //Tambahkan Parameter username dan password untuk Request
                params.put(Config.KEY_LOGIN, IdLogin);
                params.put(Config.KEY_PASSWORD, PassLogin);

                //Kembalikan Nilai parameter
                return params;
            }
        };
        //Tambahkan Request String ke dalam Queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}
