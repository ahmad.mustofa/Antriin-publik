package com.stoufam.hp.antriin;
import java.util.HashMap;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by hp on 11/30/2016.
 */

public interface RestAPI {

    @FormUrlEncoded
    @POST("tambah_teller.php")
    Call<ResponseBody> tambahTeller (@FieldMap HashMap<String, String> params);

    @GET("ambilmarker.php")
    Call<List<model_bank>> tes();

    @GET("selectmarker2.php")
    Call<List<model_detail_bank>> loadDetailBank(@QueryMap HashMap<String, String> params);

    @GET("jumlah_teller.php")
    Call<List<model_teller>> loadTeller(@QueryMap HashMap<String, String> params);

    @GET("jumlah_cs.php")
    Call<List<model_teller>> loadCs(@QueryMap HashMap<String, String> params);

    @GET("detail_antrian.php")
    Call<List<model_detail_antrian>> loadAntrian(@QueryMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("update_antrian.php")
    Call<ResponseBody> updateAntrian (@FieldMap HashMap<String, String> params);

    @GET("selectpelanggan.php")
    Call<List<model_pelanggan>> loadPelanggan(@QueryMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("update_pelanggan.php")
    Call<ResponseBody> updateProfil (@FieldMap HashMap<String, String> params);

//    @GET("sepedaku/get_profil.php")
//    Call<ModelProfil> loadProfil(@QueryMap HashMap<String, String> params);
//
//    //update profil
//    @FormUrlEncoded
//    @POST("sepedaku/update_profil.php")
//    Call<ResponseBody> editProfil (@FieldMap HashMap<String, String> params);
//
//    @GET("sepedaku/get_sepeda.php")
//    Call<ModelSepeda> loadSepeda(@QueryMap HashMap<String, String> params);
//
//    //update sepeda
//    @FormUrlEncoded
//    @POST("sepedaku/update_sepeda.php")
//    Call<ResponseBody> editSepeda (@FieldMap HashMap<String, String> params);
//
//    @GET("sepedaku/get_stnk.php")
//    Call<ModelStnk> loadStnk(@QueryMap HashMap<String, String> params);
}

