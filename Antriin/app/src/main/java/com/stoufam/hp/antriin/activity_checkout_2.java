package com.stoufam.hp.antriin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


import static com.stoufam.hp.antriin.Config.ROOT_URL;

public class activity_checkout_2 extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private NavigationView navigationView;
    private Menu menu;
    private Toolbar toolbar;
    private TextView[] textViews = new TextView[6];
    private LinearLayout cancel;

    String nim = "11";
    String nomor_antrian = "nomor";
    String jenis = "0";
    boolean status = true;

    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout_3);

        kosong();
        setup_actionbar();

    }

    void proses_konversi() {
        if (jenis.equals("1")) {
            textViews[4].setText("TELLER");
            textViews[5].setText("A" + nomor_antrian);
        } else if (jenis.equals("2")) {
            textViews[4].setText("CUSTOMER SERVICE");
            textViews[5].setText("B" + nomor_antrian);
        }
    }

    void alert() {
        // Munculkan alert dialog apabila user ingin keluar aplikasi
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setMessage("Apakah Anda Yakin Untuk Membatalkan ?");
        alertDialogBuilder.setPositiveButton("YA",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        updateDataRetrofit();
                        ShowData();

//                        // Getting out
//                        SharedPreferences preferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
//                        //Getting editor
//                        SharedPreferences.Editor editor = preferences.edit();
//
//                        // put nilai false untuk login
//                        editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, false);
//
//                        // put nilai untuk username
//                        editor.putString(Config.EMAIL_SHARED_PREF, "");
//
//                        //Simpan ke haredpreferences
//                        editor.commit();
//
//                        // Balik dan tampilkan ke halaman Utama aplikasi jika logout berhasil
////                        Intent intent = new Intent(MainActivity.this, Login.class);
////                        startActivity(intent);

//                        Intent launchNextActivity;
//                        launchNextActivity = new Intent(getApplicationContext(), activity_main.class);
//                        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                        startActivity(launchNextActivity);
                    }
                });
        // Pilihan jika NO
        alertDialogBuilder.setNegativeButton("TIDAK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        // Tampilkan alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    void kosong() {

        inisialisasi();

        nim = new class_prosessql(this).bacadatamember()[0].toString();

//        new class_http().set_profil_new(this, textViews, circleImageView, new class_prosessql(this).bacadatamember()[0]);
        ShowData();
    }

    void inisialisasi() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (new class_prosessql(this).cek_session().equals("ada")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_drawer_logged);
        }
        navigationView.setNavigationItemSelectedListener(this);
    }

    void isi() {
        setContentView(R.layout.activity_checkout_2);

        inisialisasi();

        setup_actionbar();

        textViews[0] = (TextView) findViewById(R.id.antrian_nama_bank);
        textViews[1] = (TextView) findViewById(R.id.jalan_bank);
        textViews[2] = (TextView) findViewById(R.id.antrian_tanggal_bank);
        textViews[3] = (TextView) findViewById(R.id.antrian_jam_bank);
        textViews[4] = (TextView) findViewById(R.id.antrian_jenis);
        textViews[5] = (TextView) findViewById(R.id.antrian_nomor);

        cancel = (LinearLayout) findViewById(R.id.layout_cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert();
            }
        });
    }

    private void ShowData() {
        //Ketika Aplikasi mengambil data kita akan melihat progress dialog
        final ProgressDialog loading = ProgressDialog.show(activity_checkout_2.this, "Fetching Data", "Please Wait..", false, false);
        loading.setCancelable(true);
        //Logging Interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set Level Log
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())//GsonConverter untuk parsing json
                .client(httpClient.build())
                .build();

        HashMap<String, String> params = new HashMap<>();
        params.put("ID_PELANGGAN", nim);

        RestAPI service = retrofit.create(RestAPI.class);
        Call<List<model_detail_antrian>> call = service.loadAntrian(params);
        call.enqueue(new Callback<List<model_detail_antrian>>() {  //Asyncronous Request
            @Override
            public void onResponse(Call<List<model_detail_antrian>> call, Response<List<model_detail_antrian>> response) {
                loading.dismiss();

                status = response.body().equals("[]");
                if ((status) || response.body().get(0).getStatus().equals("SELESAI")) {

                    setContentView(R.layout.activity_checkout_3);
                    inisialisasi();
                    setup_actionbar();
                }
//                if (status.equals("ANTRI")) {
//                    inisialisasi2();
//
//                    textViews[0].setText(response.body().get(0).getNamaBank());
//                    textViews[1].setText(response.body().get(0).getAlamat());
//                    textViews[2].setText(response.body().get(0).getTanggal());
//                    textViews[3].setText(response.body().get(0).getJam());
//                    jenis = response.body().get(0).getJenis();
//                    nomor_antrian = response.body().get(0).getNomorAntrian();
//
//                    proses_konversi();

                else {
                    isi();

                    textViews[0].setText(response.body().get(0).getNamaBank());
                    textViews[1].setText(response.body().get(0).getAlamat());
                    textViews[2].setText(response.body().get(0).getTanggal());
                    textViews[3].setText(response.body().get(0).getJam());
                    jenis = response.body().get(0).getJenis();
                    nomor_antrian = response.body().get(0).getNomorAntrian();

                    proses_konversi();
                }
            }

            @Override
            public void onFailure(Call<List<model_detail_antrian>> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(getApplicationContext(), "Gagal mengambil data", Toast.LENGTH_LONG).show();
            }
        });
    }

    void updateDataRetrofit() {
        final String nim = new class_prosessql(this).bacadatamember()[0];

        HashMap<String, String> params = new HashMap<>();
        params.put("ID_PELANGGAN", nim);

        //Ketika Aplikasi mengambil data kita akan melihat progress dialog
        final ProgressDialog loading = ProgressDialog.show(this, "Update Data", "Silahkan Tunggu...", false, false);
        loading.setCancelable(true);
        //Logging Interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set Level Log
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())//GsonConverter untuk parsing json
                .client(httpClient.build())
                .build();

        RestAPI api = retrofit.create(RestAPI.class);
        Call<ResponseBody> result = api.updateAntrian(params);
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loading.dismiss();
                try {
                    if (response.body() != null)
                        ShowData();
                    snackbar = Snackbar.make(findViewById(R.id.drawer_layout), "Antrian Terhapus", Snackbar.LENGTH_LONG);
                    snackbar.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(activity_checkout_2.this, "Proses gagal", Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
    }

    void setup_actionbar() {
        setSupportActionBar(toolbar);
        ActionBar ab = getSupportActionBar();
        TextView tv = new TextView(getApplicationContext());

        getSupportActionBar().setTitle("My Antrian");
        // Create a LayoutParams for TextView
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView
        // Apply the layout parameters to TextView widget
        tv.setLayoutParams(lp);

        // Set text to display in TextView
        tv.setText(ab.getTitle()); // ActionBar title text

        // Set the text color of TextView to black
        // This line change the ActionBar title text color
        tv.setTextColor(Color.WHITE);

        // Set the TextView text size in dp
        // This will change the ActionBar title text size
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);

        // Set the ActionBar display option
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        // Finally, set the newly created TextView as ActionBar custom view
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        class_menu.optiondipilih(menu, id, activity_checkout_2.this);
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, activity_main.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        class_menu.menudipilih(id, activity_checkout_2.this);
        return true;
    }
}
