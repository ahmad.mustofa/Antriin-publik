package com.stoufam.hp.antriin;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class activity_about extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private Toolbar tbar;
    private NavigationView navigationView;
    private Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        inisialisasi();
        set_title();
    }

    void inisialisasi() {
        class_helpersqlite db_helper = new class_helpersqlite(this);
        SQLiteDatabase db = db_helper.getWritableDatabase();
        db_helper.create_member(db);

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (new class_prosessql(this).cek_session().equals("ada")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_drawer_logged);
        } else {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.menu_drawer);
        }

        navigationView.getMenu().getItem(0).setChecked(true);
        navigationView.setNavigationItemSelectedListener(this);

        tbar = (Toolbar) findViewById(R.id.toolbar);
    }

    void set_title() {
        setSupportActionBar(tbar);
        ActionBar ab = getSupportActionBar();

        getSupportActionBar().setTitle("About");

        TextView tv = new TextView(getApplicationContext());

        // Create a LayoutParams for TextView
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT, // Width of TextView
                RelativeLayout.LayoutParams.WRAP_CONTENT); // Height of TextView

        // Apply the layout parameters to TextView widget
        tv.setLayoutParams(lp);

        // Set text to display in TextView
        tv.setText(ab.getTitle()); // ActionBar title text

        // Set the text color of TextView to black
        // This line change the ActionBar title text color
        tv.setTextColor(Color.WHITE);

        // Set the TextView text size in dp
        // This will change the ActionBar title text size
        tv.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 14);

        // Set the ActionBar display option
        ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

        // Finally, set the newly created TextView as ActionBar custom view
        ab.setCustomView(tv);
        ab.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        class_menu.menudipilih(id, activity_about.this);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, activity_main.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        class_menu.optiondipilih(menu, id, activity_about.this);
        return super.onOptionsItemSelected(item);
    }

}
