package com.stoufam.hp.antriin;

/**
 * Created by hp on 3/12/2017.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class model_teller {

    @SerializedName("id_bank")
    @Expose
    private String idBank;
    @SerializedName("jumlah_antrian")
    @Expose
    private String jumlahAntrian;

    public String getIdBank() {
        return idBank;
    }

    public void setIdBank(String idBank) {
        this.idBank = idBank;
    }

    public String getJumlahAntrian() {
        return jumlahAntrian;
    }

    public void setJumlahAntrian(String jumlahAntrian) {
        this.jumlahAntrian = jumlahAntrian;
    }

}
