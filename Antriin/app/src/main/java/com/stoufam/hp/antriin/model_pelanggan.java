package com.stoufam.hp.antriin;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by AM on 1/24/2018.
 */

public class model_pelanggan {
    @SerializedName("ID_PELANGGAN")
    @Expose
    private String iDPELANGGAN;
    @SerializedName("NAMA")
    @Expose
    private String nAMA;
    @SerializedName("NOMOR_HP")
    @Expose
    private String nOMORHP;
    @SerializedName("EMAIL")
    @Expose
    private String eMAIL;

    public String getIDPELANGGAN() {
        return iDPELANGGAN;
    }

    public void setIDPELANGGAN(String iDPELANGGAN) {
        this.iDPELANGGAN = iDPELANGGAN;
    }

    public String getNAMA() {
        return nAMA;
    }

    public void setNAMA(String nAMA) {
        this.nAMA = nAMA;
    }

    public String getNOMORHP() {
        return nOMORHP;
    }

    public void setNOMORHP(String nOMORHP) {
        this.nOMORHP = nOMORHP;
    }

    public String getEMAIL() {
        return eMAIL;
    }

    public void setEMAIL(String eMAIL) {
        this.eMAIL = eMAIL;
    }
}
