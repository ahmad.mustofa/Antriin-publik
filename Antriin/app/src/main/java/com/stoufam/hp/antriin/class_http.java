package com.stoufam.hp.antriin;

import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;

import static com.stoufam.hp.antriin.Config.ROOT_URL;

/**
 * Created by hp on 3/12/2017.
 */

public class class_http {
    public static String url = "http://poinhome.com/api/";
    private String respon;
    private Snackbar snackbar;
    public static String key = "poinhome";

    public void proses_login(final AppCompatActivity context, final String username, final String password) {
        respon = "";
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
//        params.put("api_key", key);
        params.put("EMAIL", username);
        params.put("PASSWORD", password);
        client.post(ROOT_URL + "login.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                snackbar = Snackbar.make(context.findViewById(R.id.drawer_layout), "Loading....", Snackbar.LENGTH_INDEFINITE);
                snackbar.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                try {
                    respon = new String(responseBody);
                    if (respon.equals("fail") || respon.isEmpty()) {
                        if (snackbar.isShown()) {
                            snackbar.dismiss();
                        }
                        snackbar = Snackbar.make(context.findViewById(R.id.drawer_layout), "Anda belum melakukan aktivasi akun, cek inbox/span email anda untuk melakukan aktivasi.", Snackbar.LENGTH_INDEFINITE);
                        snackbar.show();
                    } else if (respon.equals("password_salah") || respon.isEmpty()) {
                        if (snackbar.isShown()) {
                            snackbar.dismiss();
                        }
                        snackbar = Snackbar.make(context.findViewById(R.id.drawer_layout), "Username / password anda salah.", Snackbar.LENGTH_INDEFINITE);
                        snackbar.show();
                    } else {
                        JSONArray jsonArray = new JSONArray(respon);
                        String[] userdata = new String[4];
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jobj = jsonArray.getJSONObject(i);
                            userdata[0] = jobj.getString("ID_PELANGGAN");
                            userdata[1] = jobj.getString("NAMA_PELANGGAN");
                            userdata[2] = jobj.getString("NOMOR_HP");
                            userdata[3] = jobj.getString("EMAIL");
//                            userdata[4] = jobj.getString("id_akses");
//                            userdata[5] = jobj.getString("id_tipemember");
//                            userdata[6] = jobj.getString("status_verified");
                        }
                        snackbar.dismiss();

                        class_helpersqlite db_helper = new class_helpersqlite(context);
                        SQLiteDatabase db = db_helper.getWritableDatabase();
                        db_helper.delete_member(db);
                        new class_prosessql(context).insertdatamember(
                                userdata[0],
                                userdata[1],
                                userdata[2],
                                userdata[3]);
                        class_menu.menudipilih(R.id.nav_home, context);
                    }
                } catch (JSONException e) {
                    snackbar.dismiss();
                    Log.e("Respon", String.valueOf(e));
                    new AlertDialog.Builder(context)
                            .setTitle("Peringatan")
                            .setMessage("Username anda tidak terdaftar.")
                            .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .show();
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                snackbar.dismiss();
//                snackbar(context.findViewById(R.id.drawer_layout));
            }
        });
    }

    public void proses_logout(final AppCompatActivity context) {
        respon = "";
        AsyncHttpClient client = new AsyncHttpClient();
        RequestParams params = new RequestParams();
//        params.put("api_key", key);
        params.put("username", new class_prosessql(context).bacadatamember()[0]);
        client.post(ROOT_URL + "login.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                snackbar = Snackbar.make(context.findViewById(R.id.drawer_layout), "Loading....", Snackbar.LENGTH_INDEFINITE);
                snackbar.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                respon = new String(responseBody);
                if (respon.equals("fail") || respon.isEmpty()) {
                    if (snackbar.isShown()) {
                        snackbar.dismiss();
                    }
                    snackbar = Snackbar.make(context.findViewById(R.id.drawer_layout), "Ada kesalahan data", Snackbar.LENGTH_INDEFINITE);
                    snackbar.show();
                } else {
                    new AlertDialog.Builder(context)
                            .setTitle("Informasi")
                            .setMessage("Apakah anda ingin keluar")
                            .setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    new class_prosessql(context).hapusdatamember();
                                    context.finish();
                                    new class_menu().menudipilih(R.id.nav_home, context);
                                    dialog.dismiss();
                                }
                            })
                            .setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                    if (snackbar.isShown()) {
                        snackbar.dismiss();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if (snackbar.isShown()) {
                    snackbar.dismiss();
                }
//                snackbar(context.findViewById(R.id.drawer_layout));
            }
        });
    }
}
