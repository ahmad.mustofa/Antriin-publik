package com.stoufam.hp.antriin;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.util.Calendar;

import static com.stoufam.hp.antriin.Config.ROOT_URL;

public class fragment_teller extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private TextView textView[] = new TextView[7];
    private String username = "username";
    String title = "";
    String akses = "";
    String member = "";
    private String mParam1;
    private String mParam2;

    String nim = "";
    String id_bank = "";
    Integer antrian = 0;

    private OnFragmentInteractionListener mListener;

    private LinearLayout pesan;

    public fragment_teller() {
        // Required empty public constructor
    }

    public static fragment_teller newInstance(String param1, String param2) {
        fragment_teller fragment = new fragment_teller();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        Intent intent = getActivity().getIntent();
        nim = new class_prosessql(getContext()).bacadatamember()[0];
        id_bank = intent.getStringExtra(Config.EMP_ID);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_teller, container, false);
        textView[0] = (TextView) view.findViewById(R.id.jumlah_antrian_teller);
        textView[1] = (TextView) view.findViewById(R.id.estimasi_waktu_teller);

//        new class_http().fragment_about_me(getActivity(),textView, new class_prosessql(getActivity()).bacadatamember()[0]);

        ShowData();
        estimasi();

        pesan = (LinearLayout) view.findViewById(R.id.layout_daftar);
        pesan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert();
            }
        });
        return view;
    }

    void estimasi() {
        Integer estimasi = antrian * 10;
        textView[1].setText(String.valueOf(estimasi) + " Menit");
    }

    void alert() {
        // Munculkan alert dialog apabila user ingin keluar aplikasi
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getContext());
        alertDialogBuilder.setMessage("Apakah Anda Yakin Mengambil Antrian Teller ?");
        alertDialogBuilder.setPositiveButton("YA",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                        if (new class_prosessql(getContext()).cek_session().equals("ada")) {
                            Tambah();
                        }else{
                            Intent a = new Intent(getContext(), activity_login.class);
                            startActivity(a);
                        }

//                        // Getting out
//                        SharedPreferences preferences = getSharedPreferences(Config.SHARED_PREF_NAME, Context.MODE_PRIVATE);
//                        //Getting editor
//                        SharedPreferences.Editor editor = preferences.edit();
//
//                        // put nilai false untuk login
//                        editor.putBoolean(Config.LOGGEDIN_SHARED_PREF, false);
//
//                        // put nilai untuk username
//                        editor.putString(Config.EMAIL_SHARED_PREF, "");
//
//                        //Simpan ke haredpreferences
//                        editor.commit();
//
//                        // Balik dan tampilkan ke halaman Utama aplikasi jika logout berhasil
////                        Intent intent = new Intent(MainActivity.this, Login.class);
////                        startActivity(intent);

//                        Intent launchNextActivity;
//                        launchNextActivity = new Intent(getActivity(), activity_checkout_2.class);
//                        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                        launchNextActivity.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
//                        startActivity(launchNextActivity);
                    }
                });
        // Pilihan jika NO
        alertDialogBuilder.setNegativeButton("TIDAK",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                    }
                });

        // Tampilkan alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void ShowData() {
        //Ketika Aplikasi mengambil data kita akan melihat progress dialog
        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Fetching Data", "Please Wait..", false, false);
        loading.setCancelable(true);
        //Logging Interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set Level Log
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())//GsonConverter untuk parsing json
                .client(httpClient.build())
                .build();

        HashMap<String, String> params = new HashMap<>();
        params.put("ID_BANK", id_bank);

        RestAPI service = retrofit.create(RestAPI.class);
        Call<List<model_teller>> call = service.loadTeller(params);
        call.enqueue(new Callback<List<model_teller>>() {  //Asyncronous Request
            @Override
            public void onResponse(Call<List<model_teller>> call, Response<List<model_teller>> response) {
                loading.dismiss();

                antrian = Integer.parseInt(response.body().get(0).getJumlahAntrian());
                textView[0].setText(String.valueOf(antrian));
            }

            @Override
            public void onFailure(Call<List<model_teller>> call, Throwable t) {
                Toast.makeText(getActivity(), "Gagal mengambil data", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void Tambah() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat time = new SimpleDateFormat("h:mm a");
        String currentDateandTime = sdf.format(new Date());
        String currentTime = time.format(Calendar.getInstance().getTime());
        final String idPelanggan = new class_prosessql(getContext()).bacadatamember()[0].toString();
        final String idJenisAntrian = "1";
        final String idBank = id_bank;
        final String jamAntri = String.valueOf(currentTime);
        final String tglAntri = String.valueOf(currentDateandTime);
        final String statusAntri = "ANTRI";

        HashMap<String, String> params = new HashMap<>();
        params.put("ID_PELANGGAN", idPelanggan);
        params.put("ID_JENIS_ANTRIAN", idJenisAntrian);
        params.put("ID_BANK", idBank);
        params.put("JAM_ANTRI", jamAntri);
        params.put("TANGGAL_ANTRI", tglAntri);
        params.put("STATUS_ANTRIAN", statusAntri);

        //Ketika Aplikasi mengambil data kita akan melihat progress dialog
        final ProgressDialog loading = ProgressDialog.show(getActivity(), "Menyimpan Data", "Silahkan Tunggu...", false, true);
//        loading.setCancelable(true);
        //Logging Interceptor
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        //set Level Log
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())//GsonConverter untuk parsing json
                .client(httpClient.build())
                .build();

        RestAPI api = retrofit.create(RestAPI.class);
        Call<ResponseBody> result = api.tambahTeller(params);
        result.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                loading.dismiss();
                try {
                    if (response.body() != null) {
                        Intent intent;
                        intent = new Intent(getActivity(), activity_checkout_2.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    intent.putExtra(Config.EMP_ID, idMahasiswa);
                        startActivity(intent);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                loading.dismiss();
                Toast.makeText(getActivity(), "Anda Gagal Mendaftar", Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
