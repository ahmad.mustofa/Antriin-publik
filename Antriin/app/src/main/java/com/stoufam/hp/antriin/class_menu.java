package com.stoufam.hp.antriin;

import android.content.Intent;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.Menu;

/**
 * Dibuat oleh Sony Vaio pada 20/08/2016.
 */
public class class_menu {
    public static void menudipilih(int id, final AppCompatActivity ctx) {
        if (id == R.id.nav_home) {
            Intent intent = new Intent(ctx, activity_main.class);
            ctx.startActivity(intent);
            ctx.finish();
        } else if (id == R.id.nav_login) {
            Intent intent = new Intent(ctx, activity_login.class);
            ctx.startActivity(intent);
            ctx.finish();
        } else if (id == R.id.nav_profil) {
            Intent intent = new Intent(ctx, activity_my_profil.class);
            ctx.startActivity(intent);
            ctx.finish();
        } else if (id == R.id.nav_antri) {
            Intent intent = new Intent(ctx, activity_checkout_2.class);
            ctx.startActivity(intent);
            ctx.finish();
        } else if (id == R.id.nav_logout) {
            new class_http().proses_logout(ctx);
        } else if (id == R.id.nav_menu5) {
            Intent intent = new Intent(ctx, activity_about.class);
            ctx.startActivity(intent);
            ctx.finish();
//        } else if (id == R.id.nav_about) {
//            Intent intent = new Intent(ctx, activity_agent.class);
//            ctx.startActivity(intent);
//            ctx.finish();
//        } else if (id == R.id.nav_profil) {
//            Intent intent = new Intent(ctx, activity_halaman_14_my_profil.class);
//            ctx.startActivity(intent);
//            ctx.finish();
//        } else if (id == R.id.nav_antri) {
//            Intent intent = new Intent(ctx, activity_myproperty.class);
//            ctx.startActivity(intent);
//            ctx.finish();
//        } else if (id == R.id.nav_menu10) {
//            Intent intent = new Intent(ctx, activity_myproject.class);
//            ctx.startActivity(intent);
//            ctx.finish();
//        } else if (id == R.id.nav_menu11) {
//            Intent intent = new Intent(ctx, activity_friend.class);
//            ctx.startActivity(intent);
//            ctx.finish();
//        } else if (id == R.id.nav_menu15) {
//            Intent intent = new Intent(ctx, activity_event_promo.class);
//            ctx.startActivity(intent);
//            ctx.finish();
//        } else if (id == R.id.nav_menu_z) {
//            Intent intent = new Intent(ctx, activity_halaman_11_detail_iklan_super_new.class);
//            ctx.startActivity(intent);
//            ctx.finish();
//        }else if (id == R.id.nav_menu_zz) {
//            Intent intent = new Intent(ctx, activity_halaman_18_setting.class);
//            ctx.startActivity(intent);
//            ctx.finish();
//        }else if (id == R.id.nav_menu_za) {
//            Intent intent = new Intent(ctx, activity_agent_new.class);
//            ctx.startActivity(intent);
//            ctx.finish();
//        }else if (id == R.id.nav_menu_aa) {
//            Intent intent = new Intent(ctx, activity_halaman_14_my_profil.class);
//            ctx.startActivity(intent);
//            ctx.finish();
//        }else if (id == R.id.nav_menu_ab) {
//            Intent intent = new Intent(ctx, activity_halaman_10_profil_detail.class);
//            ctx.startActivity(intent);
//            ctx.finish();
        }
    }

    public static void optiondipilih(Menu menu, int id, final AppCompatActivity ctx) {
        if (id == android.R.id.home) {
            DrawerLayout drawer = (DrawerLayout) ctx.findViewById(R.id.drawer_layout);
            drawer.openDrawer(Gravity.LEFT);
        }
//        else if (id == R.id.add_friend) {
//            Intent intent = new Intent(ctx, activity_add_friend.class);
//            ctx.startActivity(intent);
//            ctx.finish();
//        } else if (id == R.id.menu_hotlist) {
//            Intent intent = new Intent(ctx, activity_friend_request.class);
//            ctx.startActivity(intent);
//            ctx.finish();
//        }
    }
}
