package com.stoufam.hp.antriin;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class class_helpersqlite extends SQLiteOpenHelper {
	private static final String NAMA_DB = "bni";
	public class_helpersqlite(Context context) {
	super(context, NAMA_DB, null, 1);
	}

	@Override
	public void onCreate(SQLiteDatabase arg0) {
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
	}

	public void  delete_member(SQLiteDatabase db){
		db.execSQL("delete from tbl_member");
	}

	public void create_member(SQLiteDatabase db){
	 	db.execSQL("CREATE TABLE if not exists tbl_member (" +
				"username TEXT PRIMARY KEY," +
				"first_name TEXT," +
				"last_name TEXT," +
				"email TEXT);");
	}
	public void buat_tbl_image(SQLiteDatabase db)
	{
		db.execSQL("CREATE TABLE if not exists tbl_image (id_image INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT, url_image TEXT);");
	}


}
